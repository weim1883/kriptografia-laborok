"""Assignment 1: Cryptography for CS41 Winter 2020.

Name: <YOUR NAME>
SUNet: <SUNet ID>

Replace this placeholder text with a description of this module.
"""
import string
import base64


#################
# CAESAR CIPHER #
#################

def error_check(text, encrypt, cipherValue):
    try:
        int(cipherValue)
    except:
        return -2

    if int(cipherValue) < 1:  # only used for scytale and railfence
        return -2
    for i in text:
        if string.ascii_lowercase.__contains__(i):
            return -1
        if not string.ascii_letters.__contains__(i):
            if encrypt:
                print("The input text contains non alphabetic characters,they will be encrypted too")
                return 0
            else:
                print("The input text contains non alphabetic characters,they will be decrypted too")
                return 0
    return 1


def encrypt_caesar(plaintext):
    """Encrypt a plaintext using a Caesar cipher.

    Add more implementation details here.

    :param plaintext: The message to encrypt.
    :type plaintext: str

    :returns: The encrypted ciphertext.
    """
    # Your implementation here.
    err = error_check(plaintext, True, 1)
    if err == -1:
        return "Error.the text contains lower case letters"
    new_word = ""
    dictionary = {
    }
    j = 0
    for i in string.ascii_uppercase:
        dictionary[i] = j
        j = j + 1

    j = 0
    for i in plaintext:
        if i in string.ascii_uppercase:
            j = dictionary[i]
            new_word = new_word + string.ascii_uppercase[(j + 3) % 26]
        else:
            new_word = new_word + i
    return new_word

print(" ")
print(encrypt_caesar("0DD !T$"))
print(" ")


def decrypt_caesar(ciphertext):
    """Decrypt a ciphertext using a Caesar cipher.

    Add more implementation details here.

    :param ciphertext: The message to decrypt.
    :type ciphertext: str

    :returns: The decrypted plaintext.
    """
    # Your implementation here.
    err = error_check(ciphertext, False, 1)
    if err == -1:
        return "Error.the text contains lower case letters"
    decrypted_word = ""
    j = 0
    dictionary = {
    }
    for i in string.ascii_uppercase:
        dictionary[i] = j
        j = j + 1
    j = 0
    for i in ciphertext:
        if i in string.ascii_uppercase:
            j = dictionary.get(i)
            j = j - 3
            if 0 > j > -4:
                j = 26 + j
                decrypted_word = decrypted_word + string.ascii_uppercase[j]
            else:
                decrypted_word = decrypted_word + string.ascii_uppercase[j]
        else:
            decrypted_word = decrypted_word + i
    return decrypted_word


print(" ")
print(decrypt_caesar("A"))
print(" ")

###################
# VIGENERE CIPHER #
###################

def encrypt_vigenere(plaintext, keyword):
    """Encrypt plaintext using a Vigenere cipher with a keyword.

    Add more implementation details here.

    :param plaintext: The message to encrypt.
    :type plaintext: str
    :param keyword: The key of the Vigenere cipher.
    :type keyword: str

    :returns: The encrypted ciphertext.
    """
    # Your implementation here.
    err = error_check(plaintext, True, 1)
    if err == -1:
        return "Error the text contains lower case letters"
    j = 0
    dictionary = {
    }
    for i in string.ascii_uppercase:
        dictionary[i] = j
        j = j + 1
    j = 0
    iterator = 0
    new_word = ""
    for i in plaintext:
        poz1 = dictionary[i]
        poz2 = dictionary[keyword[iterator % len(keyword)]]
        iterator = iterator + 1
        new_word = new_word + string.ascii_uppercase[(poz1 + poz2) % 26]
    return new_word

print(" ")
print(encrypt_vigenere("ATTACKATDAWN", "LEMON"))
print(" ")


def decrypt_vigenere(ciphertext, keyword):
    """Decrypt ciphertext using a Vigenere cipher with a keyword.

    Add more implementation details here.

    :param ciphertext: The message to decrypt.
    :type ciphertext: str
    :param keyword: The key of the Vigenere cipher.
    :type keyword: str

    :returns: The decrypted plaintext.
    """
    # Your implementation here.
    err = error_check(ciphertext, False, 1)
    if err == -1:
        return "Error.the text contains lower case letters"
    j = 0
    dictionary = {
    }
    for i in string.ascii_uppercase:
        dictionary[i] = j
        j = j + 1
    j = 0
    word = ""
    iterator = 0
    for i in ciphertext:
        poz1 = dictionary[i]
        poz2 = dictionary[keyword[iterator % len(keyword)]]
        iterator = iterator + 1
        last_pos = poz1 - poz2
        if last_pos < 0:
            last_pos = 26 + last_pos
        word = word + string.ascii_uppercase[last_pos]
    return word

print(" ")
print(decrypt_vigenere("LXFOPVEFRNHR", "LEMON"))
print(" ")


def encrypt_scytale(plaintext, circumference):
    err = error_check(plaintext, True, circumference)
    if err == -1:
        return "Error.the text contains lower case letters"
    if err == -2:
        return "Wrong circumference value"
    i = 0
    j = 0
    new_word = ""
    row = 0
    while i < circumference:
        j = i
        while j < len(plaintext):
            new_word = new_word + plaintext[j]
            j = j + circumference
        i = i + 1
    return new_word

print(" ")
print(encrypt_scytale("almakortea", 5))
print(" ")


def decrypt_scytale(ciphertext, circumference):
    err = error_check(ciphertext, False, circumference)
    if err == -1:
        return "Error.the text contains lower case letters"
    if err == -2:
        return "Wrong circumference value"
    i = 0  # mindig nezem  a betuket negyenkent,eloszort 1-es indexbol indulva utana 2-es stb
    j = 0

    new_word = ""
    while i < int(len(ciphertext) / circumference):
        j = i
        while j < len(ciphertext):
            new_word = new_word + ciphertext[j]
            j = j + int(len(ciphertext) / circumference)
        i = i + 1
    return new_word

print(" ")
print(decrypt_scytale("aolrmtaeka", 5))
print(" ")

# felepitem a megadott abrat
def create_railfence_matrix(num_rails, len):
    x = []
    i = 0
    while i < num_rails:
        x.append([])
        i = i + 1
    i = 0
    j = 0
    while i < num_rails:
        while j < len:
            x[i].append('.')
            j = j + 1
        j = 0
        i = i + 1
    return x


def set_railfence_matrix_values_for_decryption(x, ciphertext, num_rails):
    i = 1
    j = 1
    x[0][0] = "*"
    while i < len(ciphertext):
        j = 1
        while j < num_rails and i < len(ciphertext):
            x[j][i] = "*"
            j = j + 1
            i = i + 1
        j = num_rails - 2
        while j > -1 and i < len(ciphertext):
            x[j][i] = "*"
            j = j - 1
            i = i + 1
    i = 0

    i = 0
    j = 0
    poz = 0
    while i < num_rails:
        while j < len(ciphertext):
            if x[i][j] == "*":
                x[i][j] = ciphertext[poz]
                poz = poz + 1
            j = j + 1
        j = 0
        i = i + 1
    return x


def set_railfence_matrix_values_for_encryption(x, plaintext, num_rails):
    i = 1
    j = 1
    poz = 1
    x[0][0] = plaintext[0]
    while poz < len(plaintext) and i < len(plaintext):
        j = 1
        while j < num_rails and poz < len(plaintext):
            x[j][i] = plaintext[poz]
            poz = poz + 1
            j = j + 1
            i = i + 1
        j = num_rails - 2
        while j > -1 and poz < len(plaintext):
            x[j][i] = plaintext[poz]
            poz = poz + 1
            j = j - 1
            i = i + 1
    return x


def encrypt_railfence(plaintext, num_rails):
    err = error_check(plaintext, True, num_rails)
    if err == -1:
        return "Error.the text contains lower case letters"
    if err == -2:
        return "Wrong circumference value"
    x = create_railfence_matrix(num_rails, len(plaintext))
    x = set_railfence_matrix_values_for_encryption(x, plaintext, num_rails)
    new_word = ""
    i = 0
    j = 0
    while i < num_rails:
        while j < len(x[i]):
            if x[i][j] != '.':
                new_word = new_word + x[i][j]
            j = j + 1
        j = 0
        i = i + 1
    return new_word


print(encrypt_railfence("WEAREDISCOVEREDFLEEATONCE", 3))


def decrypt_railfence(ciphertext, num_rails):
    err = error_check(ciphertext, False, num_rails)
    if err == -1:
        return "Error.the text contains lower case letters"
    if err == -2:
        return "Wrong circumference value"
    x = create_railfence_matrix(num_rails, len(ciphertext))
    x = set_railfence_matrix_values_for_decryption(x, ciphertext, num_rails)
    i = 0
    j = 0
    poz = 0
    new_word = ""
    i = 1
    j = 1
    new_word = new_word + x[0][0]
    while i < len(ciphertext):
        j = 1
        while j < num_rails:
            new_word = new_word + x[j][i]
            j = j + 1
            i = i + 1
        j = num_rails - 2
        while j > -1:
            new_word = new_word + x[j][i]
            j = j - 1
            i = i + 1
    return new_word

print(" ")
print(decrypt_railfence("WECRLTEERDSOEEFEAOCAIVDEN", 3))
print(" ")


def non_text_file_to_string_railfence_encryption(file_name):
    file = open(file_name, "rb")
    content = ""
    read_val = base64.b64encode(file.read())
    new_val = ""
    for i in read_val:
        new_val = new_val + chr(i)
    new_word = encrypt_railfence_binary(new_val,3)           #checking whether the encryption was done correctly or not
    decrypted_word = decrypt_railfence_binary(new_word,3)
    print(decrypted_word == new_val)
    return new_word


def encrypt_railfence_binary(plaintext, num_rails):
    x = create_railfence_matrix(num_rails, len(plaintext))
    x = set_railfence_matrix_values_for_encryption(x, plaintext, num_rails)
    new_word = ""
    i = 0
    j = 0
    while i < num_rails:
        while j < len(x[i]):
            if x[i][j] != '.':
                new_word = new_word + x[i][j]
            j = j + 1
        j = 0
        i = i + 1
    return new_word


def decrypt_railfence_binary(ciphertext, num_rails):
    x = create_railfence_matrix(num_rails, len(ciphertext))
    x = set_railfence_matrix_values_for_decryption(x, ciphertext, num_rails)
    i = 0
    j = 0
    poz = 0
    new_word = ""
    i = 1
    j = 1
    new_word = new_word + x[0][0]
    while i < len(ciphertext):
        j = 1
        while j < num_rails and i < len(ciphertext):
            new_word = new_word + x[j][i]
            j = j + 1
            i = i + 1
        j = num_rails - 2
        while j > -1 and i < len(ciphertext):
            new_word = new_word + x[j][i]
            j = j - 1
            i = i + 1
    return new_word




print(non_text_file_to_string_railfence_encryption("C:\\Users\\erich\\Desktop\\5 felev\\kripto\\Laborok\\kriptografia-laborok\\lab1\\assign1\\kep.jpg"))

########################################
# MERKLE-HELLMAN KNAPSACK CRYPTOSYSTEM #
########################################

def generate_private_key(n=8):
    """Generate a private key to use with the Merkle-Hellman Knapsack Cryptosystem.

    Following the instructions in the handout, construct the private key
    components of the MH Cryptosystem. This consists of 3 tasks:

    1. Build a superincreasing sequence `w` of length n
        Note: You can double-check that a sequence is superincreasing by using:
            `utils.is_superincreasing(seq)`
    2. Choose some integer `q` greater than the sum of all elements in `w`
    3. Discover an integer `r` between 2 and q that is coprime to `q`
        Note: You can use `utils.coprime(r, q)` for this.

    You'll also need to use the random module's `randint` function, which you
    will have to import.

    Somehow, you'll have to return all three of these values from this function!
    Can we do that in Python?!

    :param n: Bitsize of message to send (defaults to 8)
    :type n: int

    :returns: 3-tuple private key `(w, q, r)`, with `w` a n-tuple, and q and r ints.
    """
    # Your implementation here.
    raise NotImplementedError('generate_private_key is not yet implemented!')


def create_public_key(private_key):
    """Create a public key corresponding to the given private key.

    To accomplish this, you only need to build and return `beta` as described in
    the handout.

        beta = (b_1, b_2, ..., b_n) where b_i = r × w_i mod q

    Hint: this can be written in one or two lines using list comprehensions.

    :param private_key: The private key created by generate_private_key.
    :type private_key: 3-tuple `(w, q, r)`, with `w` a n-tuple, and q and r ints.

    :returns: n-tuple public key
    """
    # Your implementation here.
    raise NotImplementedError('create_public_key is not yet implemented!')


def encrypt_mh(message, public_key):
    """Encrypt an outgoing message using a public key.

    Following the outline of the handout, you will need to:
    1. Separate the message into chunks based on the size of the public key.
        In our case, that's the fixed value n = 8, corresponding to a single
        byte. In principle, we should work for any value of n, but we'll
        assert that it's fine to operate byte-by-byte.
    2. For each byte, determine its 8 bits (the `a_i`s). You can use
        `utils.byte_to_bits(byte)`.
    3. Encrypt the 8 message bits by computing
         c = sum of a_i * b_i for i = 1 to n
    4. Return a list of the encrypted ciphertexts for each chunk of the message.

    Hint: Think about using `zip` and other tools we've discussed in class.

    :param message: The message to be encrypted.
    :type message: bytes
    :param public_key: The public key of the message's recipient.
    :type public_key: n-tuple of ints

    :returns: Encrypted message bytes represented as a list of ints.
    """
    # Your implementation here.
    raise NotImplementedError('encrypt_mh is not yet implemented!')


def decrypt_mh(message, private_key):
    """Decrypt an incoming message using a private key.

    Following the outline of the handout, you will need to:
    1. Extract w, q, and r from the private key.
    2. Compute s, the modular inverse of r mod q, using the Extended Euclidean
        algorithm (implemented for you at `utils.modinv(r, q)`)
    3. For each byte-sized chunk, compute
         c' = cs (mod q)
    4. Solve the superincreasing subset sum problem using c' and w to recover
        the original plaintext byte.
    5. Reconstitute the decrypted bytes to form the original message.

    :param message: Encrypted message chunks.
    :type message: list of ints
    :param private_key: The private key of the recipient (you).
    :type private_key: 3-tuple of w, q, and r

    :returns: bytearray or str of decrypted characters
    """
    # Your implementation here.
    raise NotImplementedError('decrypt_mh is not yet implemented!')
