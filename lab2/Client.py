import Merkle_Hellman_Knapsack
import socket
import _thread
import ClientReaderThread
from random import randint
import Solitaire


class Client:
    __public_key = []
    __private_key = [], 0, 0
    __port_num = 0
    __key_from_server = []
    __sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    __client_sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    __this_half = 0
    __other_half = 0
    __common_secret = 0
    __sol = Solitaire.Solitaire(1)
    __terminate = False

    def __init__(self, inp):
        self.__sock.connect(('127.0.0.1', 8080))  # Serverrel valo kommunikaciora hasznalt port
        if inp == 1:
            print("Please write the port number used by the client")
            self.__port_num = input()
            self.__client_sock.bind(("127.0.0.1", int(self.__port_num)))
            self.__client_sock.listen(5)
            print(self.__port_num)
            self.__reader_thread = ClientReaderThread.ReaderAndWriter(self)
            _thread.start_new_thread(self.__reader_thread.run, (self.__client_sock,))

    def register_pub_key(self):
        print("Sent public key to server")
        self.__sock.send(("public " + str(self.__port_num) + " " + str(self.__public_key)).encode())
        resp = self.__sock.recv(4096)
        resp = resp.decode("utf-8")
        if resp.__contains__("r"):
            raise ValueError('Given port number was not yet registered')

    def generate_knapsack_key_pair(self):
        self.__private_key = Merkle_Hellman_Knapsack.generate_private_key()
        self.__public_key = Merkle_Hellman_Knapsack.generate_public_key(self.__private_key)
        self.__reader_thread.set_private_key(self.__private_key)
        print("Client created key pair")

    def get_public_key(self, port_num):
        print("Sent request to get public key")
        self.__sock.send(("get public " + str(port_num)).encode())
        resp = self.__sock.recv(4096)
        resp = resp.decode("utf-8")
        if resp.__contains__("r"):
            raise KeyError('Given port number was not yet registered')
        else:
            resp = resp.replace("[", "")
            resp = resp.replace("]", "")
            resp = resp.replace(" ", "")
            resp_split = resp.split(",")
            for i in resp_split:
                self.__key_from_server.append(int(i))

    def generate_half_secret(self):
        return randint(1, 20)

    def wait_for_commands(self):
        print("Client commands:\n 'Create key pair': creates a key pair \n 'Send public key:' sends public key" +
              "  to server\n 'Terminate':closes a socket\n 'Get public key port_num': get public key from server" +
              "\n 'Send hello port_num ': opens communication with other client \n 'Send half secret ':sends half" +
              "secret to other client\n 'Generate half': generate half secret \n 'Generate common secret' : " +
              "generate common secret \n 'Init solitaire':initializa solitaire\n 'Send sol message': sends solitaire " +
              "encrypted message")
        while True:
            print("Please type a command ")
            string = input()
            if string == "Create key pair":
                self.generate_knapsack_key_pair()
            if string == "Send public key":
                self.register_pub_key()
            if string == "Terminate":
                print("Closing client")
                self.__sock.close()
                self.__reader_thread.terminate()
                break
            if string == "Init solitaire":
                self.__sol.set_secret(self.__common_secret)
                self.__sol.reset_deck_values()
                print("Initialized solitaire")

            split_str = string.split(" ")
            if split_str[0] == "Send" and split_str[1] == "hello":
                port_num = int(split_str[2])
                self.__reader_thread.set_write_port(port_num)
                mess = "Hello"
                enc_list = Merkle_Hellman_Knapsack.encrypt(mess, self.__key_from_server)
                enc_message = "encrypthello "
                i = 0
                while i < len(enc_list):
                    enc_message = enc_message + str(enc_list[i]) + " "
                    i = i + 1
                self.__reader_thread.write(enc_message)

            if split_str[0] == "Get" and split_str[1] == "public":
                self.get_public_key(int(split_str[3]))

            if split_str[0] == "Send" and split_str[1] == "half":
                message = "half " + str(self.__this_half)
                self.__reader_thread.write(message)
                print("Sent half secret")
            if split_str[0] == "Generate" and split_str[1] == "half":
                self.__this_half = self.generate_half_secret()
                print("Generated half secret")

            if split_str[0] == "Generate" and split_str[1] == "common":
                self.__common_secret = self.__other_half + self.__this_half
                self.__reader_thread.set_secret(self.__common_secret)
                print("Generated common secret")
            if split_str[0] == "Send" and split_str[1] == "sol":
                j = 0
                new_data = ""
                for i in split_str:
                    if j >= 2:
                        new_data += i + " "
                    j += 1
                print("Message before encryption :" + new_data)

                string = self.__sol.encrypt(new_data)
                self.__reader_thread.write("sol " + string)
            if self.__terminate is True:
                break

    def set_other_half(self, other_half):
        self.__other_half = other_half

    def terminate(self):
        self.__terminate = True

    def set_port(self, port_num):
        self.__port_num = port_num
        self.__client_sock.bind(("127.0.0.1", int(self.__port_num)))
        self.__client_sock.listen(5)
        print(self.__port_num)
        self.__reader_thread = ClientReaderThread.ReaderAndWriter(self)
        _thread.start_new_thread(self.__reader_thread.run, (self.__client_sock,))

    def select_stored_public_key(self):
        return self.__public_key

# unit test mukodesehez a Klienset 0-val kell inicializalni illetve ki kell kommentalni a wait for commandsot meg a klienst

client = Client(1)
client.wait_for_commands()
