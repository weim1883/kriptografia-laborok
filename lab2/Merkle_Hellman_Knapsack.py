from random import randint
from math import gcd
import utils

n = 8


def bits_to_byte_conversion(bits):
    ret = 0
    h = 1
    for i in range(n):
        ret += h * bits[i]
        h *= 2
    return ret


def super_increasing():  # letrehoz egy szupernovekvo sorozatot
    val = randint(2, 10)
    vect = [val]
    i = 1
    while i < n:
        s = sum(vect)
        val = randint(s + 1, 2 * s)
        vect.append(val)
        i = i + 1
    return vect


def generate_private_key():  # letrehozza a nyilvanos es private kulcsokat
    super_increasing_sequence = super_increasing()
    s = sum(super_increasing_sequence)
    q = randint(s + 1, 2 * s)
    r = randint(2, q - 1)
    while gcd(q, r) != 1:
        r = randint(2, q - 1)
    return super_increasing_sequence, q, r


def generate_public_key(private_key):
    public_key = []
    private_k = private_key[0]
    i = 0
    while i < n:
        public_key.append((private_key[2] * private_k[i]) % private_key[1])
        i = i + 1
    return public_key


def encrypt(message, public_key):  # enkriptal egy uzenetet
    if not isinstance(message, str):
        raise TypeError("Input must be in string format")
    if len(message) < 1:
        raise ValueError("Input must contain at least one letter")
    encrypted_message = []
    for i in message:
        byte = ord(i)
        bits = utils.byte_to_bits(byte)
        j = 0
        s = 0
        while j < n:
            s = s + bits[j] * public_key[j]
            j = j + 1
        encrypted_message.append(s)
    return encrypted_message


def decrypt(message, private_key):  # dekriptal egy uzenetet
    if not isinstance(message[0], int):
        raise TypeError("Input must be in number format")
    if len(message) < 1:
        raise ValueError("Input must contain at least one letter")
    s = utils.modinv(private_key[2], private_key[1])
    i = 0;
    text = ''
    while i < len(message):
        c = message[i] * s % private_key[1]
        p = Knapsack(c, private_key[0])
        num = bits_to_byte_conversion(p)
        text = text + chr(num)
        i = i + 1
    return text


def Knapsack(V, v):
    s = V
    i = n - 1
    epsz = []
    while i >= 0:
        if v[i] <= s:
            epsz.append(1)
            s = s - v[i]
        else:
            epsz.append(0)
        i = i - 1
    if s == 0:
        return epsz
    else:
        print('Erre nincs megoldas')
