class KeyServer:
    __public_key = []
    __port_number = 0
    __is_sent = False

    def __init__(self, thread_serv):
        self.thread_serv = thread_serv

    def __set_public_key(self, text):  # feldolgozza a megkapot stringet es beallitja a public keyt
        text = text.replace("[", "")
        text = text.replace("]", "")
        text = text.replace(" ", "")
        text_list = text.split(",")
        self.__public_key = []
        for i in text_list:
            self.__public_key.append(int(i))

    def get_port(self):
        return self.__port_number

    def get_public_key(self):
        return self.__public_key

    def start(self, serv, conn, adr):
        while True:
            data = conn.recv(4096)
            data = data.decode("utf-8")
            split_text = data.split(" ")
            if split_text[0] == "public":  # feldolgozom a stringkent megkapott szoveget
                if self.__is_sent is True:
                    conn.send("Error".encode())
                else:
                    self.__port_number = int(split_text[1])
                    split_text = data.split("[")

                    self.__set_public_key(split_text[1])  # pelda megkapott string: public 8080 [1, 500,445676]
                    print("Received public key")
                    self.__is_sent = True
                    conn.send("Ok".encode())
            if split_text[0] == "get" and split_text[1] == "public":
                port = int(split_text[2])
                public_key = self.thread_serv.get_public_key_by_port(port)
                if public_key != -1:
                    print("Sending requested key")
                    conn.send(str(public_key).encode())
                else:
                    print("Sending error")
                    conn.send("Error".encode())
                    break
    def get_is_Sent(self):
        return self.__is_sent
