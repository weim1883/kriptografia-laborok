import unittest
import Solitaire
from random import randint



class test_Solitaire(unittest.TestCase):
    def test_equality(self):
        sol = Solitaire.Solitaire(13)
        self.assertEqual(sol.decrypt(sol.encrypt("szoveg")), "szoveg")
        self.assertEqual(sol.decrypt(sol.encrypt("Ab")), "Ab")
        self.assertEqual(sol.decrypt(sol.encrypt("A b")), "A b")
        i = 0 #100 random generalt szovegre (amely kis es nagy betukbol all) valo teszt
        while i < 100:
            length = randint(1,20)
            text = ""
            for j in range(length):
                text = text + chr(randint(97,122))
                text = text + chr(randint(65,90))
            self.assertEqual(sol.decrypt(sol.encrypt(text)), text)
            i = i + 1

    def test_input_type(self):
        sol = Solitaire.Solitaire(13)
        with self.assertRaises(TypeError):
            self.assertRaises(TypeError, sol.encrypt(123))
            self.assertRaises(TypeError, sol.decrypt(123))
            self.assertRaises(TypeError, sol.decrypt(123.123))

    def test_input_values(self):
        sol = Solitaire.Solitaire(13)
        with self.assertRaises(ValueError):
            self.assertRaises(ValueError, sol.encrypt("12334289gbuhvfcnxm scdert"))
            self.assertRaises(ValueError, sol.decrypt("123132"))
            self.assertRaises(ValueError, sol.decrypt(""))
