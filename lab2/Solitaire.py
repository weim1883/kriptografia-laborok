import string
import numpy


class Solitaire:
    __dictionary = {
    }  # char -> szam megfeleltetesre
    __is_first_joker = 0
    __out_card = 0

    # alap felallasa a kartyaknak
    def reset_deck_values(self):
        numpy.random.seed(self.__secret)
        self.__deck = numpy.random.permutation(54) + 1
        self.__is_first_joker = 0
        self.__out_card = 0

    def __init__(self, secret):
        self.__secret = secret
        numpy.random.seed(self.__secret)
        self.__deck = numpy.random.permutation(54) + 1
        j = 0
        for i in string.ascii_uppercase:
            self.__dictionary[i] = j
            j = j + 1
        j = 0
        for i in string.ascii_lowercase:
            self.__dictionary[i] = j
            j = j + 1

    def __swap(self, deck, i, j):
        help = deck[i]
        deck[i] = deck[j]
        deck[j] = help

    def __find(self, deck, val):
        i = 0
        while i < len(deck):
            if deck[i] == val:
                return i
            i = i + 1
        return -1

    def __copy_deck(self, deck):
        i = 0
        while i < len(deck):
            self.__deck[i] = deck[i]
            i = i + 1

    def __move_joker_A(self):  # ha lehet swappolok ha nem akkor csusztatom a tombot
        for i in range(len(self.__deck)):
            if self.__deck[i] == 53:
                self.__swap(self.__deck, i, (i + 1) % len(self.__deck))
                break

    def __move_joker_B(self):
        for i in range(len(self.__deck)):
            if self.__deck[i] == 54:
                self.__swap(self.__deck, i, (i + 1) % len(self.__deck))
                self.__swap(self.__deck, (i + 1) % len(self.__deck), (i + 2) % len(self.__deck))
                break

    def __triple_cut(self):
        new_deck = []
        p1 = self.__find(self.__deck, 53)
        p2 = self.__find(self.__deck, 54)
        joker1 = 0
        joker2 = 0
        if p1 < p2:
            joker1 = p1
            joker2 = p2
        else:
            joker1 = p2
            joker2 = p1
        i = joker2 + 1
        while i < len(self.__deck):
            new_deck.append(self.__deck[i])
            i = i + 1
        i = joker1
        while i <= joker2:
            new_deck.append(self.__deck[i])
            i = i + 1
        i = 0
        while i < joker1:
            new_deck.append(self.__deck[i])
            i = i + 1
        self.__copy_deck(new_deck)

    def __count_cut(self):
        bottom_card = self.__deck[53]  # bottom_cardnyi elemet rakok a pakli vegere a pakli elejerol
        if bottom_card != 53 and bottom_card != 54:
            new_deck = []
            i = bottom_card
            while i < len(self.__deck) - 1:  # bemasolom az elemet ay uj deckbe kiveve ay utolsot
                new_deck.append(self.__deck[i])
                i = i + 1
            i = 0
            while i < bottom_card:  # hozzarakom az elejet
                new_deck.append(self.__deck[i])
                i = i + 1
            new_deck.append(bottom_card)

            self.__copy_deck(new_deck)

    def __output_card(self):
        top_card = self.__deck[0]
        if top_card != 53 and top_card != 54:
            self.__out_card = self.__deck[top_card + 1]
            self.__is_first_joker = 0
        else:
            self.__is_first_joker = 1

    def __create_output_card(self):
        self.__move_joker_A()
        self.__move_joker_B()
        self.__triple_cut()
        self.__count_cut()
        self.__output_card()
        while self.__is_first_joker == 1:
            self.__move_joker_A()
            self.__move_joker_B()
            self.__triple_cut()
            self.__count_cut()
            self.__output_card()

    def __get_charachter_of_value(self, value, is_upper):
        if is_upper == 0:
            for i in string.ascii_lowercase:
                if self.__dictionary[i] == value:
                    return i
        else:
            for i in string.ascii_uppercase:
                if self.__dictionary[i] == value:
                    return i

    def encrypt(self, message):
        encrypted_text = ""
        self.__input_test(message)
        for i in message:
            if i != " ":
                self.reset_deck_values()
                self.__create_output_card()
                card_val = self.__out_card
                card_val = (card_val + self.__dictionary[i]) % 26
                if string.ascii_lowercase.__contains__(i):
                    character = self.__get_charachter_of_value(card_val, 0)
                else:
                    character = self.__get_charachter_of_value(card_val, 1)
                encrypted_text = encrypted_text + character
            else:
                encrypted_text = encrypted_text + " "
        return encrypted_text

    def decrypt(self, message):
        decrypted_text = ""
        self.__input_test(message)
        for i in message:
            if i != " ":
                self.reset_deck_values()
                self.__create_output_card()
                card_val = self.__out_card
                card_val = (self.__dictionary[i] - card_val) % 26
                if string.ascii_lowercase.__contains__(i):
                    character = self.__get_charachter_of_value(card_val, 0)
                else:
                    character = self.__get_charachter_of_value(card_val, 1)
                decrypted_text = decrypted_text + character
            else:
                decrypted_text = decrypted_text + " "
        return decrypted_text

    def __input_test(self, message):
        if not isinstance(message, str):
            raise TypeError("Input must be in string format")

        if len(message) < 1:
            raise TypeError("Input must contain at least a charachter")

        for i in message:
            if not (string.ascii_lowercase.__contains__(i) or string.ascii_uppercase.__contains__(i) or " ".__eq__(i)):
                raise ValueError("Input must only contain letters")

    def set_secret(self, secret):
        self.secret = secret
