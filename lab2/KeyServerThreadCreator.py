import socket
import KeyServer
import _thread


class KeyServerThreadCreator:
    __server_list = []

    def get_public_key_by_port(self, port):  # adott porttal rendelkezo kezeloben tarolt publikus kulcsot kuldi vissza
        for i in self.__server_list:
            if i.get_port() == port:
                return i.get_public_key()
        return -1
    def run(self):
        serv = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        serv.bind(('127.0.0.1', 8080))
        serv.listen(5)

        while True:
            conn, adr = serv.accept()
            key_serv = KeyServer.KeyServer(self)
            _thread.start_new_thread(key_serv.start, (serv, conn, adr))
            self.__server_list.append(key_serv)
        conn.close()


key_serv = KeyServerThreadCreator()
key_serv.run()
