import socket
import Merkle_Hellman_Knapsack
import Solitaire


class ReaderAndWriter:
    __port = 0
    __sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    __private_key = []
    __hello_sent = False
    __half_got = False
    __secret = 0
    __sol = Solitaire.Solitaire(1)

    def __init__(self, client):
        self.__client = client

    def run(self, sock):
        while True:
            conn, adr = sock.accept()
            while True:
                data = conn.recv(4096)
                data = data.decode("utf-8")
                data_split = data.split(" ")
                if data_split[0] == "encrypthello":
                    encoded_values = []
                    i = 1
                    while i < len(data_split) - 1:
                        encoded_values.append(int(data_split[i]))
                        i = i + 1
                    text = Merkle_Hellman_Knapsack.decrypt(encoded_values, self.__private_key)
                    if text == "Hello":
                        print("Received hello")
                        self.__hello_sent = True
                if data_split[0] == "half" and self.__hello_sent is True:
                    print("Received half secret")
                    half = int(data_split[1])
                    self.__client.set_other_half(half)
                    self.__half_got = True
                if data_split[0] == "sol" and self.__hello_sent is True and self.__half_got is True:
                    i = 1
                    new_data = ""
                    while i < len(data_split):
                        new_data += data_split[i] + " "
                        i += 1
                    decrypted_text = self.__sol.decrypt(new_data)
                    print("After decryption ", decrypted_text)

                    while True:
                        data = conn.recv(4096)
                        data = data.decode("utf-8")

                        data_split = data.split(" ")
                        i = 1
                        new_data = ""
                        while i < len(data_split):
                            new_data = new_data + data_split[i] + " "
                            i += 1
                        decrypted_text = self.__sol.decrypt(new_data)
                        print("After decryption", decrypted_text)
                        decrypted_text.replace(" ","")
                        if decrypted_text .__eq__("Bye"):
                            print("benne")
                            self.__sock.close()
                            self.__client.terminate()
                            break

                    break
                    self.__client.terminate()

    def set_write_port(self, port):  # amikor mar tudjuk hpgy kivel fog kommunikalni a kliens beallitjuk a portjat
        self.__port = port
        self.__sock.connect(('127.0.0.1', self.__port))  # Serverrel valo kommunikaciora hasznalt port

    def write(self, message):
        self.__sock.send(message.encode())

    def terminate(self):
        self.__sock.close()

    def set_private_key(self, priv_key):
        self.__private_key = priv_key

    def set_secret(self, secret):
        self.__secret = secret
        self.__sol.set_secret(self.__secret)
        self.__sol.reset_deck_values()
