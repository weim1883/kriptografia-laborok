import unittest
import Merkle_Hellman_Knapsack
from random import randint


class TestMerkleHellman(unittest.TestCase):
    def test_equality(self):
        priv_key = Merkle_Hellman_Knapsack.generate_private_key()
        pub_key = Merkle_Hellman_Knapsack.generate_public_key(priv_key)
        self.assertEqual(
            Merkle_Hellman_Knapsack.decrypt(Merkle_Hellman_Knapsack.encrypt('Ez egy mondat kellene legyen', pub_key),
                                            priv_key),
            'Ez egy mondat kellene legyen')
        self.assertEqual(
            Merkle_Hellman_Knapsack.decrypt(Merkle_Hellman_Knapsack.encrypt('A', pub_key),
                                            priv_key),
            'A')

        i = 0  # 100 random generalt szovegre valo teszt
        while i < 100:
            length = randint(1, 20)
            text = ""
            for j in range(length):
                text = text + chr(randint(0, 127))
            self.assertEqual(Merkle_Hellman_Knapsack.decrypt(Merkle_Hellman_Knapsack.encrypt(text, pub_key), priv_key),
                             text)
            i = i + 1

    def test_number_input(self):
        priv_key = Merkle_Hellman_Knapsack.generate_private_key()
        pub_key = Merkle_Hellman_Knapsack.generate_public_key(priv_key)
        with self.assertRaises(TypeError):
            self.assertRaises(TypeError,
                              Merkle_Hellman_Knapsack.decrypt(Merkle_Hellman_Knapsack.encrypt(123, pub_key), priv_key),
                              123)
            self.assertRaises(TypeError,
                              Merkle_Hellman_Knapsack.decrypt(Merkle_Hellman_Knapsack.encrypt(123.1, pub_key),
                                                              priv_key), 123.1)
            self.assertRaises(TypeError,
                              Merkle_Hellman_Knapsack.decrypt("Asfgsdhif",
                                                              priv_key), 123.1)

    def test_input_length(self):
        priv_key = Merkle_Hellman_Knapsack.generate_private_key()
        pub_key = Merkle_Hellman_Knapsack.generate_public_key(priv_key)
        with self.assertRaises(ValueError):
            self.assertRaises(ValueError,
                              Merkle_Hellman_Knapsack.decrypt(Merkle_Hellman_Knapsack.encrypt("", pub_key), priv_key),
                              123)
            self.assertRaises(ValueError,
                              Merkle_Hellman_Knapsack.decrypt(Merkle_Hellman_Knapsack.encrypt("asd", pub_key),
                                                              priv_key),
                              123)
