import unittest
import Client


class test_KeyServer(unittest.TestCase):
    def test_key_reregistration(self):
        cl = Client.Client(0)
        cl.set_port(8082)
        cl.generate_knapsack_key_pair()
        cl.register_pub_key()
        self.assertRaises(ValueError, cl.register_pub_key)
        self.assertRaises(KeyError, cl.get_public_key, 8089)
        cl.terminate()

